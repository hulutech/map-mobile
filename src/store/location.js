import {defineStore} from 'pinia'
import axios from '@/utils/axios.js'

export default defineStore('location', {
    state: () => {
        return {
            positions: []
        }
    },
    actions: {
        async addPosition(position) {
            this.positions.push(position)
            await axios.post("/position", position)
        },
        async deletePosition(position) {
            await axios.delete(`/position/${position.id}`)
            const index = this.positions.findIndex(item => item.uuid === position.uuid)
            if (index !== -1) {
                this.positions.splice(this.positions.indexOf(position), 1)
            }
        },
        async getPositions() {
            return await axios.get("/position")
        },
        async updatePosition(position) {
            await axios.put(`/position/${position.id}`, position)
            const index = this.positions.findIndex(item => item.uuid === position.uuid)
            if (index !== -1) {
                this.positions.splice(index, 1, position)
            }
        },
        findPosition(uuid) {
            const index = this.positions.findIndex(item => item.uuid === uuid)
            if (index !== -1) {
                return this.positions[index]
            } else {
                return null
            }
        }
    },
})
