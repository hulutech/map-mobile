import {defineStore} from 'pinia'
import axios from '@/utils/axios.js'

export default defineStore('admin', {
    state: () => {
        return {
            user: {mobile: "", password: ""},
            token: ""
        }
    },
    actions: {
        setLogin(data) {
            this.user = data.user
            this.token = data.token
            localStorage.setItem('user', JSON.stringify(data.user))
            localStorage.setItem('token', data.token)
        },
        isLogin(user) {
            return user.name === this.user.name && user.password === this.user.password
        },
        setAuth() {
            this.isAuth = 1
            localStorage.setItem('auth', 1)
        },
        getToken() {
            return localStorage.getItem('token')
        },
        setBgImg(path) {
            //     设置背景图片
            let img = document.querySelector("#img")
            img.src = path
        },
        getAuth() {
            let user = JSON.parse(localStorage.getItem('user'))
            if (user) {
                return user.role === 'admin'
            } else {
                return false
            }
        },
        logout() {
            localStorage.clear()
            //     刷新页面
            window.location.reload()
        },
        async loadBgImg() {
            return await axios.get("bg/img")
        }
    },
})
