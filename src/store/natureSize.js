import {defineStore} from 'pinia'
// 图片的原始尺寸
export default defineStore('natureSize', {
    state: () => ({
        sizeWidth: 0,
        sizeHeight: 0,
        truthForRate: null,
        imgRate: 0,
    }),
    getters: {
        getNatureSize() {
            return {
                sizeWidth: this.sizeWidth,
                sizeHeight: this.sizeHeight,
            };
        },
    },
    actions: {
        setNatureSize(sizeWidth, sizeHeight) {
            this.sizeWidth = sizeWidth
            this.sizeHeight = sizeHeight
        },
        setRate(truthForRate) {
            this.truthForRate = truthForRate
            localStorage.setItem('truthForRate', truthForRate)
        },
        getRate() {
            return localStorage.getItem('truthForRate')
        },
        setImgRate(imgRate) {
            this.imgRate = imgRate
            localStorage.setItem('imgRate', imgRate)
        },
        getImgRate() {
            return this.imgRate === 0 ? localStorage.getItem('imgRate') : this.imgRate
        }
    },
})
