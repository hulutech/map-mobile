import {defineStore} from 'pinia'

export default defineStore('pattern', {
    state: () => {
        return {
            modal: "view"
        }
    },
    getters: {
        getPattern() {
            return this.modal
        },
    },
    actions: {
        setPattern(modal) {
            this.modal = modal
        },
    },
})
