import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        component: () => import('@/components/Panel.vue'),
    },
    {
        path: '/admin',
        component: () => import('@/components/admin.vue'),
    }
]

const router = createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: createWebHashHistory(),
    routes, //
})
// before
router.beforeEach((to, from, next) => {
    if (to.path === '/') {
        const token = window.localStorage.getItem('token')
        if (token) {
            next()
        } else {
            next({ path: '/admin' })
        }
    } else {
        next()
    }
})
export default router