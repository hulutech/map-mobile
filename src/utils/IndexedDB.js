// 定义一个class,该class用于对indexedDB进行操作
class DBOpera {
    db = null
    dbName = null
    dbVersion = null
    dbStoreName = null

// 构造函数
    constructor(dbName, dbVersion, dbStoreName) {
        this.dbName = dbName;
        this.dbVersion = dbVersion;
        this.dbStoreName = dbStoreName;
    }

//     打开数据库
    openDB() {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     创建数据库
    createDB() {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     删除数据库
    deleteDB() {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     创建表
    createTable() {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     删除表
    deleteTable() {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     添加数据
    addData(data) {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }

    }

//     删除数据
    deleteData(id) {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     修改数据
    updateData(id, data) {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     查询数据
    queryData(id) {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     查询所有数据
    queryAllData() {
        // 打开数据库
        let request = window.indexedDB.open(this.dbName, this.dbVersion);
        // 打开数据库成功
        request.onsuccess = function (event) {
            this.db = event.target.result;
        }
    }

//     关闭数据库
    closeDB() {
        this.db.close();
    }
}

export default DBOpera