class node {
    /**
     * 相对视口x坐标
     * @type {number}
     * @private
     */
    _clientX = 0;
    /**
     * 相对视口Y坐标
     * @type {number}
     * @private
     */
    _clientY = 0;
    /**
     * 相对图片x坐标
     * @type {number}
     * @private
     */
    _picX = 0;
    /**
     * 相对图片y坐标
     * @type {number}
     * @private
     */
    _picY = 0;

    _content = "";

    constructor() {
    }

    getClientX() {
        return this._clientX;
    }

    setClientX(value) {
        this._clientX = value;
    }

    getClientY() {
        return this._clientY;
    }

    setClientY(value) {
        this._clientY = value;
    }

    getPicX() {
        return this._picX;

    }

    setPicX(value) {
        this._picX = value;
    }

    getPicY() {
        return this._picY;

    }

    setPicY(value) {
        this._picY = value;
    }

    setContent(value) {
        this._content = value;
    }
    getContent() {
        return this._content;
    }
}