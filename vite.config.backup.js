import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { join } from "path";
// https://vitejs.dev/config/
let fileName = 'dist/mobile'

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': join(__dirname, "src"),
    },
  },
  base: 'dist/mobile', // 这里更改打包相对绝对路径
  proxy: {
    '/api': {
      target: 'http://map.test',
      changeOrigin: true,
      rewrite: path => {
        path = path.replace(/^\/api/, '')
        return path
      }
    }
  },
  server: {
    host: '0.0.0.0',
  },
  build: {
    outDir: fileName,
    assetsPublicPath: './'
  }
})
